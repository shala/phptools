<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2018/12/28 0028
 * Time: 下午 19:23
 */
require_once __DIR__. '/units/function.php';
opcache_reset(); //清理缓存
swoole_set_process_name("update_master");
$http = new swoole_http_server("0.0.0.0", 3010);
$http->set(array(
    'worker_num' => 1,
));
$http->on("start", function ($server) {
    echo "已启动...";
});
$http->on("request", function ($request, $response) {
    $_GET = $request->get;
    $_POST = $request->post;
    $_COOKIE = $request->cookie;
    $_FILES = $request->files;
    $_SERVER['query'] = query_str($request->server['query_string']);
    $url = $request->server['request_uri'];
    preg_match_all('/\/[a-z]+/', $url, $matches);
    $pathName = isset(current($matches)[1]) ? current($matches)[1] : '/index';
    $file = __DIR__ .'/app'. $pathName .'.php';
    $result = 'hohoho~';
    if(is_file($file)) {
        require_once $file;
        $className = substr($pathName, 1, 10);
        $router = new $className($request);
        $result = $router->initPage();
    } else {}
    $response->header("Content-Type", "text/plain");
    $response->end($result);
});
$http->start();
