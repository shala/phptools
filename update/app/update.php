<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2018/12/28 0028
 * Time: 下午 19:51
 */
class update {
    public $request;
    public $input;
    public function __construct($request){
        $this->request = $request;
        $this->input = $_SERVER['query'];
    }
    public function initPage () {
        $hotName = $this->input['hook'];
        $file = dirname(__DIR__) . '/config/';
        $phps = $file.'php/'.$hotName.'.json';
        if(is_file($phps)) {
            require_once dirname(__DIR__).'/copy/copy.php';
            $copy = new copy();
            $jsonData = @file_get_contents($phps);
            $arrData = json_decode($jsonData, true);
            $this->nodeCmd($arrData['beforeShell']);
            $result = $copy->index($arrData);
            $this->nodeCmd($arrData['afterShell']);
            return json_encode($result);
        }
    }
    public function nodeCmd ($exc) {
        $result = '';
        foreach($exc as $v) {
            print_r(exec($v,$result).PHP_EOL);
        }
        return $result;
    }
}
