<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2018/12/28 0028
 * Time: 下午 20:09
 */
function query_str($str=''){
    $data = [];
    foreach (explode('&', $str) as $v) {
        $str = explode('=', $v);
        $data[$str[0]] = $str[1];
    }
    return $data;
}
