<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2018/12/29 0029
 * Time: 上午 10:34
 */

require_once dirname(__DIR__).'/units/FileUtil.php';
 class copy {
     public function __construct(){}
     public function index ($arr) {
         if(is_array($arr)) {
             return $this->start($arr);
         }
     }
     public function start ($arr) {
         $fu = new FileUtil();
         $firdir = $arr['firstDir'];
         $lastDir = $arr['lastDir'];
         $result = [];
         foreach ($arr['list'] as $d) {
             $path = $firdir.$d;
             $lastFile = $lastDir.$d;
             if(is_file($path)) {
                 print_r('文件'. $path.PHP_EOL);
                 print_r('被复制到:'.$lastDir.PHP_EOL);
                 $res = $arr['type'] == 'copy' ? $fu->copyFile($path, $lastFile, true) : $fu->moveFile($path, $lastFile, true);
             } else {
                 print_r('目录'. $path.PHP_EOL);
                 print_r('被复制到:'.$lastDir.PHP_EOL);
                 $res = $arr['type'] == 'copy' ? $fu->copyDir($path, $lastFile, true) : $fu->moveDir($path, $lastFile, true);
             }
             array_push($result, $res);
         }
         return $result;
     }
 }
